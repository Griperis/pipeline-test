FROM python:3.6-alpine

WORKDIR /usr/src/app

#COPY requirements.txt /usr/src/app/
RUN apk update && apk add curl && apk add wget
#RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app
